# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

* Test blanding av norsk og utenlandsk kontaktadresse.
* Underkommandoen `kontaktadr`: Legg til informasjon om hvilke adresser som finnes, og hvilken som ble valgt.
* Implementer flere underkommandoer.
* Gjør det mulig å kjøre applikasjonen som en modul.
* `kontaktadr`: Legg til felt `navnMedStorForbokstav'.


## Changed

* Med `--fregoppl` behøves ikke noen innfil.


### Fixed

* Rett feil hvor etternavn ikke kom med dersom attributtet `forkortetNavn` ikke  finnes.
* Rett feil hvor landkode ikke kom med.


## [1.0.0-beta] - 2022-10-06

### Added

- Fjern alt som ikke er tall, før validering av fødsels- og D-nummer.
- Implementer underkommandoen `regstatus`.
- Gjør det mulig å velge skilletegn i CSV-filer.
- Gjør det mulig å legge til tidsstempel i utfilens filnavn.


## Changed

- Flytt funksjonalitet fra underkommandoen `kontaktadr` til funksjoner.
- Gjør hjelp for kommandoen litt mer oversiktlig.


<!--
Added for new features.
Changed for changes in existing functionality.
Deprecated for soon-to-be removed features.
Removed for now removed features.
Fixed for any bug fixes.
Security in case of vulnerabilities.
-->
