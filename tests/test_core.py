import logging

import dynaconf  # type: ignore
import pytest
import requests


class TestLogging:
    @pytest.fixture(scope="class")
    def config(self) -> dynaconf.base.LazySettings:
        return dynaconf.Dynaconf(
            settings_files=["config.toml", ".secrets.toml"],
            environments=True,
            merge_enabled=True,
        )

    # def test_verbositet(self, config: dynaconf.base.LazySettings):
    #     runner = CliRunner(cli.fregbulk, ["-v"])
    #     assert config.logging.loggers[""].level == "WARNING"
    #     runner = CliRunner(cli.fregbulk, ["-vv"])
    #     assert config.logging.loggers[""].level == "INFO"
    #     runner = CliRunner(cli.fregbulk, ["-vvv"])
    #     assert config.logging.loggers[""].level == "DEBUG"

    def test_endepunkt(self, config):
        headers = {
            "x-gravitee-api-key": config.freg_api.api_noekkel,
            "accept": "application/json",
        }
        p = requests.post(
            config.freg_api.url
            + "v1/personer/bulkoppslag?somBestilt=true&part=person-basis",
            headers=headers,
            json={"foedselsEllerDNummer": ["29129319060"]},
            timeout=60,
        )
        logging.debug(p.__dict__)
        logging.debug(p.request.__dict__)
        assert p.status_code == 200

        g = requests.get(
            config.freg_api.url + "v1/personer/29129319060",
            headers=headers,
            timeout=60,
        )
        logging.debug(g.__dict__)
        logging.debug(g.request.__dict__)
        assert g.status_code == 200
