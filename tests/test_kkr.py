import logging

import dynaconf  # type: ignore
import pytest

from fregbulk import core, kkr


class TestKKRKlient:
    @pytest.fixture(scope="class")
    def config(self) -> dynaconf.base.LazySettings:
        return dynaconf.Dynaconf(
            settings_files=["config.toml", ".secrets.toml"],
            environments=True,
            merge_enabled=True,
        )

    @pytest.fixture(scope="class")
    def klient(self, config) -> kkr.GraviteeKlient:
        return kkr.GraviteeKlient(
            url=config.kkr_api.url, api_noekkel=config.kkr_api.api_noekkel
        )

    def test_oppslag(self, klient: kkr.GraviteeKlient):
        p = klient.post_v1_personer(
            personidentifikatorer=["00000000000", "29129319060"]
        )
        import json

        logging.debug(json.dumps(p.json()))
        assert p.status_code == 200

    def test_legg_til_oppl_fra_kkr(self, klient: kkr.GraviteeKlient) -> None:
        personer_freg = [
            {"foedselsEllerDNummer": "00000000000", "status": "???"},
            {"foedselsEllerDNummer": "29129319060", "status": "!!!"},
        ]
        personer_begge = core.legg_til_oppl_fra_kkr(klient, personer_freg)

        oensket_resultat = [
            {
                "foedselsEllerDNummer": "00000000000",
                "status": "???",
                "kkr_reservasjon": "",
                "kkr_spraak": "",
                "kkr_status": "IKKE_REGISTRERT",
                "kkr_varslingsstatus": "KAN_IKKE_VARSLES",
                "kkr_epostadresse": "",
                "kkr_epostadresse_oppdatert": "",
                "kkr_epostadresse_sist_verifisert": "",
                "kkr_mobiltelefonnummer": "",
                "kkr_mobiltelefonnummer_oppdatert": "",
                "kkr_mobiltelefonnummer_sist_verifisert": "",
            },
            {
                "foedselsEllerDNummer": "29129319060",
                "status": "!!!",
                "kkr_reservasjon": "NEI",
                "kkr_spraak": "nn",
                "kkr_status": "AKTIV",
                "kkr_varslingsstatus": "KAN_VARSLES",
                "kkr_epostadresse": "29129319060-test@minid.norge.no",
                "kkr_epostadresse_oppdatert": "2023-03-07T05:55:05+01",
                "kkr_epostadresse_sist_verifisert": "2023-03-07T05:55:05+01",
                "kkr_mobiltelefonnummer": "+4799999999",
                "kkr_mobiltelefonnummer_oppdatert": "2023-03-07T05:55:05+01",
                "kkr_mobiltelefonnummer_sist_verifisert": "2023-03-07T05:55:05+01",  # noqa: E501
            },
        ]

        assert personer_begge == oensket_resultat
