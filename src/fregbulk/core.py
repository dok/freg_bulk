import csv
import json
import logging
import re
from copy import deepcopy
from math import ceil
from typing import List, TextIO

import requests

from fregbulk import kkr

noekler_som_skal_slettes = [
    "ajourholdstidspunkt",
    "erGjeldende",
    "kommunenummer",
    "gyldighetstidspunkt",
    "bruksenhetsnummer",
    "bruksenhetstype",
    "adressekode",
    "adresseIdentifikatorFraMatrikkelen",
    "flyttedato",
    "grunnkrets",
    "stemmekrets",
    "skolekrets",
    "kirkekrets",
    "oppholdsadressedato",
]


def generer_parametre(
    entiteter: list, som_bestilt: bool = True  # noqa: FBT001, FBT002
) -> str:
    """Generer parametre for API-kall.

    `somBestilt` er som standard true."""
    entitet_noekkel = "part="
    utsnitt = entitet_noekkel + ("&" + entitet_noekkel).join(entiteter)
    if som_bestilt:
        som_bestilt_verdi = "somBestilt=true&"
    else:
        som_bestilt_verdi = ""
    return "?" + som_bestilt_verdi + utsnitt


def del_opp_liste(inndata_filtrert):
    """Del opp lister med mer enn 1000 fødselsnummer."""
    inndata_filtrert_oppdelt = []
    last = 0
    for i in range(1000, len(inndata_filtrert) + 1000, 1000):
        inndata_filtrert_oppdelt.append(
            {"foedselsEllerDNummer": inndata_filtrert[last:i]}
        )
        last = i
    logging.info(
        str(len(inndata_filtrert) + 1)
        + " fnr. delt opp i "
        + str(ceil(len(inndata_filtrert) / 1000))
        + " liste(r)."
    )
    return inndata_filtrert_oppdelt


def api_kall_bulk(
    endepunkt: str, api_noekkel: str, parametre: str, fnr_liste: list[str]
):  # -> request,models.Response:
    """Utfør API-kall."""
    headers = {"x-gravitee-api-key": api_noekkel, "accept": "application/json"}

    logging.info("Del opp liste i bolker med maks 1000 fnr./D-nr. …")
    oppdelt_liste = del_opp_liste(fnr_liste)

    logging.info("Utfør API-kall …")
    for idx, i in enumerate(oppdelt_liste):
        p = requests.post(
            endepunkt + parametre, json=i, headers=headers, timeout=60
        )
        if p.status_code == 200:  # noqa: PLR2004
            logging.debug(
                f"API-kall {idx+1} av {len(oppdelt_liste)} var vellykket."
            )
            if idx == 0:
                p_dict = p.json()
            else:
                p_dict["oppslag"].extend(p.json()["oppslag"])
        else:
            logging.error(f"{p.status_code=} {p.content=}")
            raise Exception
    return p_dict


def hent_fregoppl(
    endepunkt: str,
    api_noekkel: str,
    entiteter: list[str],
    inndata: list[str],
    fil_med_oppl: TextIO,
):
    """Hent folkeregisteropplysninger fra JSON-fil eller fra API."""
    if fil_med_oppl:
        logging.info("Last folkeregisteropplysninger fra JSON-fil …")
        personer = json.load(fil_med_oppl)["oppslag"]
        return personer, None
    else:
        logging.info("Hent folkeregisteropplysninger med API-kall …")
        parametre = generer_parametre(entiteter)
        p_dict = api_kall_bulk(
            endepunkt + "v1/personer/bulkoppslag",
            api_noekkel,
            parametre,
            inndata,
        )
        personer = deepcopy(p_dict["oppslag"])
        return personer, p_dict


def slett_felt(d: dict, d_navn: str, skal_slettes: list) -> dict:
    """Slett objekter med navn fra «skal_slettes».

    Sletter rekursivt objekter med gitte navn i en dict som inneholder dict-,
    list- eller str-objekter

    NB: Kan ikke slette nøkler i lister
    """
    ut = {}
    for k, v in d.items():
        if isinstance(d[k], (bool, str)):
            if k not in skal_slettes:
                ut.update({k: v})
        elif isinstance(d[k], dict):
            ut.update(slett_felt(d[k], k, skal_slettes))
        elif isinstance(d[k], list):
            ut.update({k: v})
    return {d_navn: ut}


def velg_aktuelle_felt(fregpers: dict, noekler_som_skal_slettes: list) -> dict:
    """Velger et utvalg objekter under 'folkeregisterperson'.

    - Velger bare dict-objekter med en nøkkel 'erGjeldende' = True.
    - Utelater nøkler i listen 'nøkler_som_skal_slettes'.
    - Fjerner list-nivå under hver 'part'-dict
    """
    ut = {}
    for k, _v in fregpers.items():
        if k != "identifikasjonsnummer":
            for i in fregpers[k]:
                if isinstance(i, dict):
                    if i["erGjeldende"] is True:
                        ut.update(slett_felt(i, k, noekler_som_skal_slettes))
                elif isinstance(i, str):  # brukes for doedsfall
                    if fregpers[k]["erGjeldende"] is True:
                        ut.update(
                            slett_felt(
                                fregpers[k], k, noekler_som_skal_slettes
                            )
                        )
                else:
                    ukjent_objekt(k, i)
    return ut


def fjern_unoedvendige_felt(personer: dict) -> dict:
    """Fjern unødvendige felt i resultatet fra folkeregisteret."""
    logging.info("Fjern unødvendige felt i resultatet …")
    for person in personer:
        for key, _value in person.items():
            if key == "folkeregisterperson":
                person[key] = velg_aktuelle_felt(
                    person[key], noekler_som_skal_slettes
                )
    return personer


def hent_evt_verdi(dict_obj: dict, noekkel: str, foranstilt: str = "") -> str:
    """Velg verdien til en nøkkel hvis den finnes, ellers en tom streng.

    Når nøkkelen finnes, kan en streng `foranstilt` foranstilles returverdien.
    """
    if noekkel in dict_obj:
        return foranstilt + dict_obj[noekkel]
    else:
        return ""


def ukjent_objekt(p1, p2):
    """Skriv en feilmelding og innholdet til to parametre.

    Brukes i else-setninger for å sjekke on noen objekter er utelatt.
    """
    logging.warning(f"Ukjente objekter: {p1=} {p2=}")


def formater_opplysninger(personer: dict, entiteter: List[str]) -> list:
    """Formater opplysninger for alle personer fra `velg_aktuelle_felt`.

    Adresselinjer returneres som en liste.
    """
    personer_formatert = []
    for person in personer:
        pers_formatert = {}

        # Skriv entiteten foedselsEllerDNummer til `pers_formatert`
        pers_formatert["foedselsEllerDNummer"] = person["foedselsEllerDNummer"]

        tom_postadr = {
            "adresselinjer": [""],
            "postnummer": None,
            "poststed": None,
            "adressegradering": None,
        }

        # Gå gjennom alle entitetene under 'folkeregisterperson'.

        # Håndtering av feilmelding eller død person
        if "feilmelding" in person.keys():
            logging.warning(f"Feilmelding: {person=}")
            pers_formatert = {
                "foedselsEllerDNummer": person["foedselsEllerDNummer"],
                "status": next(iter(person["feilmelding"].keys()))
                + ": "
                + next(iter(person["feilmelding"].values())),
            }
            if "bostedsadresse" in entiteter:
                pers_formatert = pers_formatert | {
                    "navn": {
                        "fornavn": None,
                        "mellomnavn": None,
                        "etternavn": None,
                        "forkortetNavn": None,
                    },
                    "postadresse": tom_postadr,
                }
            elif "doedsfall" in entiteter:
                pers_formatert = pers_formatert | {"doedsdato": None}
            else:
                logging.warning(f"Ukjent objekt: {person=}")
        elif (
            "doedsfall" in entiteter
            and person["folkeregisterperson"]["status"]["status"] == "doed"
        ):
            logging.debug(f"Dødsfall: {person=}")
            if "navn" in person["folkeregisterperson"].keys():
                pers_formatert = (
                    {"foedselsEllerDNummer": person["foedselsEllerDNummer"]}
                    | person["folkeregisterperson"]
                    | {"postadresse": tom_postadr}
                )
            else:
                pers_formatert = {
                    "foedselsEllerDNummer": person["foedselsEllerDNummer"]
                } | {
                    k: person["folkeregisterperson"][k]
                    for k in set(person["folkeregisterperson"].keys())
                    - {"doedsfall"}
                }
            if "bostedadresse" in entiteter:
                pers_formatert = pers_formatert | {
                    "navn": {
                        "fornavn": None,
                        "mellomnavn": None,
                        "etternavn": None,
                        "forkortetnavn": None,
                    },
                    "postadresse": tom_postadr,
                }
            elif "doedsfall" in entiteter:
                pers_formatert = (
                    pers_formatert | person["folkeregisterperson"]["doedsfall"]
                )
            else:
                logging.warning(f"Ukjent objekt: {person=}")
            pers_formatert["status"] = pers_formatert["status"]["status"]
        else:
            for k, v in person["folkeregisterperson"].items():
                entitet = person["folkeregisterperson"][k]
                if k in entiteter:
                    ret_entitet: dict[str, str | list[str]] = {}  # adresse
                    for k2, v2 in entitet.items():
                        if k == "navn":
                            ret_entitet = {
                                "fornavn": hent_evt_verdi(v, "fornavn"),
                                "mellomnavn": hent_evt_verdi(v, "mellomnavn"),
                                "etternavn": hent_evt_verdi(v, "etternavn"),
                                "forkortetNavn": hent_evt_verdi(
                                    v, "forkortetNavn"
                                ),
                            }
                            # TODO: Også stor forbokstav etter bindestrek.
                            navn_stor = (
                                hent_evt_verdi(v, "fornavn")
                                + hent_evt_verdi(v, "mellomnavn", " ")
                                + hent_evt_verdi(v, "etternavn", " ")
                            )
                            maks_lengde_navn_digital_post = 130
                            if len(navn_stor) <= maks_lengde_navn_digital_post:
                                ret_entitet[
                                    "navnMedStorForbokstav"
                                ] = " ".join(
                                    i.capitalize() for i in navn_stor.split()
                                )
                            else:
                                logging.warning(
                                    "Navn for langt for digital post "
                                    f"(maks {maks_lengde_navn_digital_post} "
                                    f"tegn):\n{navn_stor} ({len(navn_stor)} "
                                    "tegn)."
                                )
                                ret_entitet["navnMedStorForbokstav"] = ""
                            """TODO: Forkort navnet til maks 130 tegn
                            gen = (i for i in reversed(list(enumerate(
                                n_l[:-1]))) if len(i) > 1)
                            for k3 in ['mellomnavn', 'fornavn']
                                if len(forkortetNavn2) > 130:
                                    if len([i for i in ret_entitet[
                                    k3] if len(i) > 1]) > 0:
                                        gen = (i for i in reversed(list(
                                            enumerate(ret_entitet[k3])))
                                            if len(i) > 1)
                                        neste = gen.__next__()
                            def forkort_ord(ordliste):
                                gen = (i for i in reversed(list(enumerate(
                                ordliste))) if len(i) > 1)
                                # Lag kopi av ordliste og tøm originalen
                                ordliste_kopi = list(ordliste)
                                ordliste = []
                                for idx, i in enumerate(ordliste_kopi):
                                    if idx == neste[0]:
                                        tmp_navn.append(neste[1][:1])
                                    else:
                                        tmp_navn.append(i)
                                forkortetNavn2 = tmp_navn
                            """
                        elif k == "status":
                            ret_entitet = v["status"]
                        # Ikke skriv bostedadresse med 'ukjentBosted' is True.
                        elif (
                            k == "bostedsadresse"
                            and "ukjentBosted" in v.keys()
                        ):
                            ret_entitet = v
                        elif k2 in ["vegadresse", "matrikkeladresse"]:
                            vegadr = entitet[k2]
                            ret_entitet["adresselinjer"] = [
                                hent_evt_verdi(vegadr, "adressenavn")
                            ]
                            for k3, v3 in vegadr.items():
                                if k3 == "coAdressenavn":
                                    if len(v3) != 0:
                                        ret_entitet.setdefault(  # type: ignore
                                            "adresselinjer", []
                                        ).insert(0, "c/o " + v3)
                                elif k3 == "adressenummer":
                                    adrnr = vegadr[k3]
                                    ret_entitet[
                                        "adresselinjer"
                                    ][  # type: ignore
                                        0
                                    ] += hent_evt_verdi(
                                        adrnr, "husnummer", " "
                                    ) + hent_evt_verdi(
                                        adrnr, "husbokstav"
                                    )
                                elif k3 == "adressetilleggsnavn":
                                    ret_entitet[
                                        "adresselinjer"
                                    ].append(  # type: ignore
                                        hent_evt_verdi(
                                            vegadr[k3], "adressetilleggsnavn"
                                        )
                                    )
                                elif k3 == "poststed":
                                    ret_entitet[
                                        "postnummer"
                                    ] = "" + hent_evt_verdi(
                                        vegadr[k3], "postnummer"
                                    )
                                    ret_entitet["poststed"] = hent_evt_verdi(
                                        vegadr[k3], "poststedsnavn"
                                    )
                                elif k3 not in ["adressenavn"]:
                                    ukjent_objekt(k3, v3)
                        elif k2 == "postboksadresse":
                            pbadr = entitet[k2]
                            for k3, v3 in pbadr.items():
                                if k3 == "postboks":
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).append(
                                        "PB "
                                        + hent_evt_verdi(pbadr, "postboks")
                                    )
                                elif k3 == "postbokseier":
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).insert(0, "c/o " + pbadr["postbokseier"])
                                elif k3 == "adresselinje":
                                    ret_entitet[k3] = v3
                                elif k3 == "poststed":
                                    ret_entitet[
                                        "postnummer"
                                    ] = "" + hent_evt_verdi(
                                        pbadr[k3], "postnummer"
                                    )
                                    ret_entitet["poststed"] = hent_evt_verdi(
                                        pbadr[k3], "poststedsnavn"
                                    )
                                else:
                                    ukjent_objekt(k3, v3)
                        elif k2 == "postadresseIFrittFormat":
                            padr_ff = entitet[k2]
                            for k3, v3 in padr_ff.items():
                                if k3 == "adresselinje":
                                    ret_entitet["adresselinjer"] = v3
                                elif k3 == "poststed":
                                    ret_entitet[
                                        "postnummer"
                                    ] = "" + hent_evt_verdi(
                                        padr_ff[k3], "postnummer"
                                    )
                                    ret_entitet["poststed"] = hent_evt_verdi(
                                        padr_ff[k3], "poststedsnavn"
                                    )
                                else:
                                    ukjent_objekt(k3, v3)
                        elif k2 in [
                            "utenlandskAdresse",
                            "postadresseIUtlandet",
                            "utenlandskAdresseIFrittFormat",
                        ]:
                            uladr = entitet[k2]
                            for k3, v3 in uladr.items():
                                if k3 == "coAdressenavn" and len(v3) != 0:
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).insert(0, "c/o " + v3)
                                elif k3 in [
                                    "adressenavn",
                                    "distriktsnavn",
                                    "region",
                                    "bygning",
                                    "etasjenummer",
                                    "boenhet",
                                    "postboks",
                                ]:
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).append(v3)
                                elif k3 == "adresselinje":
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).extend(uladr[k3])
                                elif k3 == "postkode":
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).append(hent_evt_verdi(uladr, "postkode"))
                                elif k3 == "byEllerStedsnavn":
                                    ret_entitet.setdefault(  # type: ignore
                                        "adresselinjer", []
                                    ).append(
                                        hent_evt_verdi(
                                            uladr, "byEllerStedsnavn"
                                        )
                                    )
                                elif k3 == "landkode":
                                    ret_entitet["landkode"] = v3
                                else:
                                    ukjent_objekt(k3, v3)
                        elif k2 == "adressenErUkjent":
                            ret_entitet[k2] = v2
                        elif k2 == "adressegradering":
                            ret_entitet["adressegradering"] = v2
                        else:
                            ukjent_objekt(k2, v2)
                            logging.warning(
                                f'{person["foedselsEllerDNummer"]=}'
                            )
                    pers_formatert[k] = ret_entitet
                else:
                    ukjent_objekt(k, v)
            if "doedsfall" in entiteter:
                pers_formatert = pers_formatert | {"doedsdato": None}
        personer_formatert.append(pers_formatert)
    return personer_formatert


def velg_og_formater_kontaktadr(personer: list, entiteter: List[str]) -> list:
    """Velger kontaktadressen og gir den et enhetlig adresseformat."""

    # Entitetene i resultatet er disse pluss den med kontaktadressen.
    if "status" in entiteter:
        entiteter_kontaktadr = [
            "foedselsEllerDNummer",
            "status",
            "fornavn",
            "mellomnavn",
            "etternavn",
            "forkortetNavn",
            "navnMedStorForbokstav",
        ]
    else:
        entiteter_kontaktadr = [
            "foedselsEllerDNummer",
            "fornavn",
            "mellomnavn",
            "etternavn",
            "forkortetNavn",
            "navnMedStorForbokstav",
        ]

    tom_postadr = {
        "adresselinjer": [""],
        "postnummer": None,
        "poststed": None,
        "adressegradering": None,
    }

    ut = []  # utdata fra funksjonen

    # Fjern alle adresser utenom kontaktadresse; finn maks antall adr.linjer.
    personer_med_kadr = []
    maks_ant_adrl = 0

    # Identifiser kontaktadresse og skriv personobjektet med denne adressen
    # til `person_med_kadr`.
    for idx, i in enumerate(personer):
        person_med_kadr = {}
        aktuelle_entiteter = list(entiteter_kontaktadr)

        # Identifiser kontaktadresse.
        # `-4`: ["fornavn", "mellomnavn", "etternavn", "forkortetNavn",
        # "navnMedStorForbokstav"] er samlet under nøkkelen "navn".
        if len(list(i.keys())) == len(entiteter_kontaktadr) - 4:
            i["postadresse"] = tom_postadr
            aktuelle_entiteter.append("postadresse")
        elif "postadresse" in i.keys():
            aktuelle_entiteter.append("postadresse")
        elif (
            "oppholdsadresse" in i.keys()
            and "adressenErUkjent" not in i["oppholdsadresse"].keys()
        ):
            aktuelle_entiteter.append("oppholdsadresse")
        elif (
            "bostedsadresse" in i.keys()
            and "ukjentBosted" not in i["bostedsadresse"].keys()
        ):
            aktuelle_entiteter.append("bostedsadresse")
        elif "postadresseIUtlandet" in i.keys():
            aktuelle_entiteter.append("postadresseIUtlandet")
        # Hvis det ikke finnes noen kjent adresse:
        elif (
            "bostedsadresse" in i.keys()
            and "ukjentBosted" in i["bostedsadresse"].keys()
        ):
            if len(i["bostedsadresse"]["ukjentBosted"]) > 1:
                ukjent_objekt(idx, i)
            bostedkommune = hent_evt_verdi(
                i["bostedsadresse"], "bostedskommune", ": bostedskommune: "
            )
            i["bostedsadresse"] = {
                "adresselinjer": ["ukjentBosted" + bostedkommune],
                "postnummer": None,
                "poststed": None,
                "adressegradering": None,
            }
            aktuelle_entiteter.append("bostedsadresse")
        else:
            ukjent_objekt(idx, i)

        # if nr. 1: Flytt fornavn, mellomnavn, etternavn til øverste nivå.
        # if nr. 2: Finn maks antall adresselinjer.
        #   Dette må gjøres før adressene kan formateres.
        for i2 in aktuelle_entiteter:
            if i2 in aktuelle_entiteter[2 : len(entiteter_kontaktadr)]:
                person_med_kadr[i2] = i["navn"].get(i2)
            else:
                person_med_kadr[i2] = i.get(i2)

        # Andre betingelse nødvendig ved status «utflyttet»
        if i2 in aktuelle_entiteter[-1] and "adresselinjer" in i[i2].keys():
            ant_adrl = len(person_med_kadr[i2]["adresselinjer"])
            if ant_adrl > maks_ant_adrl:
                maks_ant_adrl = ant_adrl

        personer_med_kadr.append(person_med_kadr)

    # Formaterer kontaktadresse fra `personer_formatert` enhetlig.
    # Fjern informasjon om adressetype og formater kontaktadresse med
    # adresselinjer.
    for person_med_kadr in personer_med_kadr:
        adroppl_d = person_med_kadr[list(person_med_kadr.keys())[-1]]
        adroppl_k = adroppl_d.keys()

        # antall adresselinjer
        if "adresselinjer" in adroppl_k:
            ant_adrl = len(adroppl_d["adresselinjer"])
        else:
            ant_adrl = 0

        ant_tomme_adrl = maks_ant_adrl - ant_adrl

        person_med_kadr_med_adrl = {}

        # De første elementene er len(entiteter_kontaktadr) + 1 adrl. Legg dem
        # til `person_med_kadr_med_adrl`
        for k in list(person_med_kadr.keys())[: len(entiteter_kontaktadr)]:
            person_med_kadr_med_adrl[k] = person_med_kadr[k]

        # Legg til adrl. som ikke er tomme.
        for adrl in range(0, ant_adrl):
            person_med_kadr_med_adrl[
                "adresselinje" + str(adrl + 1)
            ] = adroppl_d["adresselinjer"][adrl]

        # Legg til evt. tomme adrl.
        while ant_tomme_adrl > 0:
            person_med_kadr_med_adrl[
                "adresselinje" + str(1 + maks_ant_adrl - ant_tomme_adrl)
            ] = None
            ant_tomme_adrl -= 1

        adrnavn = list(person_med_kadr.keys())[-1]
        # Legg til linjer etter adrl
        for k2 in ["postnummer", "poststed", "landkode", "adressegradering"]:
            if k2 in person_med_kadr[adrnavn].keys():
                person_med_kadr_med_adrl[k2] = person_med_kadr[adrnavn][k2]
            else:
                person_med_kadr_med_adrl[k2] = None

        ut.append(person_med_kadr_med_adrl)

    return ut


def legg_til_oppl_fra_kkr(
    kkr_klient: kkr.Klient, freg_personer: list[dict[str, str]]
) -> list[dict[str, str]]:
    """Legg til opplysninger fra KKR."""
    kkr_kontaktinfo = [
        "epostadresse",
        "epostadresse_oppdatert",
        "epostadresse_sist_verifisert",
        "mobiltelefonnummer",
        "mobiltelefonnummer_oppdatert",
        "mobiltelefonnummer_sist_verifisert",
    ]
    kkr_kontakt_tuppel = [(i, "kkr_" + i) for i in kkr_kontaktinfo]

    kkr_metadata = [
        # 'personidentifikator',
        "reservasjon",
        "spraak",
        # 'spraak_oppdatert',
        "status",
        "varslingsstatus",
    ]
    kkr_metadata_tuppel = [(i, "kkr_" + i) for i in kkr_metadata]

    foerste_i_utsnitt = 0
    differensial = 1000
    while True:
        utsnitt_fnr = [
            i["foedselsEllerDNummer"]
            for i in freg_personer[
                foerste_i_utsnitt : foerste_i_utsnitt + differensial
            ]
        ]
        if len(utsnitt_fnr) == 0:
            break
        p = kkr_klient.post_v1_personer(utsnitt_fnr)
        if [
            i["personidentifikator"] for i in p.json()["personer"]
        ] != utsnitt_fnr:
            logging.error("Alt blir feil!")
        for idx, kkr_person in enumerate(p.json()["personer"]):
            for kkr_oppl in kkr_metadata_tuppel:
                freg_personer[foerste_i_utsnitt + idx][
                    kkr_oppl[1]
                ] = hent_evt_verdi(kkr_person, kkr_oppl[0])
            if "kontaktinformasjon" not in kkr_person:
                kkr_person["kontaktinformasjon"] = {}
            for kkr_oppl in kkr_kontakt_tuppel:
                freg_personer[foerste_i_utsnitt + idx][
                    kkr_oppl[1]
                ] = hent_evt_verdi(
                    kkr_person["kontaktinformasjon"], kkr_oppl[0]
                )
        foerste_i_utsnitt += differensial
    return freg_personer


def skriv_resultat(
    til_csv: dict, til_json: dict, utfil, format_, skilletegn_ut
):
    """Skriv resultat til utfil som csv eller json."""
    filendelse = utfil.name.split(".")[-1].lower()
    if filendelse == ".json" or format_ == "json":
        json.dump(til_json, utfil)
    elif filendelse == ".csv" or format_ == "csv":
        keys = til_csv[0].keys()
        dict_writer = csv.DictWriter(utfil, keys, delimiter=skilletegn_ut)
        dict_writer.writeheader()
        dict_writer.writerows(til_csv)


def main(
    sett_av_opplysninger,
    innfil,
    utfil,
    med_oppl_fra_kkr,
    format_,
    fregoppl,
    # verbose,
    skilletegn_inn,
    skilletegn_ut,
    config,
):
    # --innfil, --fregoppl
    # Les fra stdin eller fil med mindre FREGOPPL er angitt
    # https://stackoverflow.com/a/53541456
    inndata_filtrert = []
    if not fregoppl:
        logging.info(f"Les fødsels- og D-nummer fra filen {innfil.name}")
        # TODO: Bruk dette et annet sted: inndata = innfil.read().rstrip()
        inndata_l = list(csv.reader(innfil, delimiter=skilletegn_inn))
        logging.debug(
            "Sjekk hvilke verdier i første kolonne som består av 11 siffer "
            "…"
        )
        for i in inndata_l:
            evt_fnr = re.subn("[^0-9]", "", i[0])[0]
            if len(evt_fnr) == 11 and evt_fnr.isdigit():  # noqa: PLR2004
                inndata_filtrert.append(str(evt_fnr))
        if len(inndata_filtrert) == 0:
            msg = "Fant ingen fødsels- eller D-nummer i inndata."
            raise ValueError(msg)
        logging.info(
            f"Innholdet i {len(inndata_filtrert)} av {len(inndata_l)} rad(er) "
            "(inkl. evt. kolonnetittel) er et fødsels- eller D-nr."
        )

    endepunkt = config.freg_api.url
    api_noekkel = config.freg_api.api_noekkel

    # subcommand
    if sett_av_opplysninger == "navn_status_kontaktadr":
        entiteter = [
            "navn",
            "status",
            "bostedsadresse",
            "oppholdsadresse",
            "deltBosted",
            "postadresse",
            "postadresseIUtlandet",
        ]
    elif sett_av_opplysninger == "navn_kontaktadr":
        entiteter = [
            "navn",
            "bostedsadresse",
            "oppholdsadresse",
            "deltBosted",
            "postadresse",
            "postadresseIUtlandet",
        ]
    elif sett_av_opplysninger == "regstatus_doedsdato":
        entiteter = ["status", "doedsfall"]

    personer, p_dict = hent_fregoppl(
        endepunkt,
        api_noekkel,
        entiteter,
        inndata=inndata_filtrert,
        fil_med_oppl=fregoppl,
    )

    personer = fjern_unoedvendige_felt(personer)

    logging.info("Formater opplysninger …")
    utdata = formater_opplysninger(personer, entiteter)

    if sett_av_opplysninger in ["navn_status_kontaktadr", "navn_kontaktadr"]:
        logging.info(
            "Velg kontaktadresse og formaterer adresser på enhetlig vis …"
        )
        utdata = velg_og_formater_kontaktadr(utdata, entiteter)

        if med_oppl_fra_kkr:
            kkr_klient = kkr.GraviteeKlient(
                url=config.kkr_api.url, api_noekkel=config.kkr_api.api_noekkel
            )
            utdata = legg_til_oppl_fra_kkr(kkr_klient, utdata)

    skriv_resultat(utdata, p_dict, utfil, format_, skilletegn_ut)
