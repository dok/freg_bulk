import logging

import requests
import uplink  # type: ignore


@uplink.json
class Klient(uplink.Consumer):
    """Klient for kontakt- og reservasjonsregisterets REST-API

    https://oidc-ver2.difi.no//kontaktinfo-oauth2-server/swagger-ui/index.html
    """

    def post_v1_personer(
        self,
        personidentifikatorer: list[str],
    ) -> requests.Response:
        """Hent ut 1 til 1000 personar frå KRR."""
        maks_personer = 1000
        if not (1 <= len(personidentifikatorer) <= maks_personer):
            logging.warning(
                f"Du forsøker å hente ut {len(personidentifikatorer)}. "
                "Tallet skal være mellom 1 og 1000."
            )
        return self.post_v1_personer_(
            nyttelast={
                "personIdentifiersAsString": "string",
                "personidentifikatorer": personidentifikatorer,
            }
        )

    @uplink.post("v1/personer")
    def post_v1_personer_(  # type: ignore
        self, nyttelast: uplink.Body
    ) -> requests.Response:
        """Hent ut 1 til 1000 personar frå KRR."""
        pass


class GraviteeKlient(Klient):
    def __init__(self, url: str, api_noekkel: str):
        super().__init__(
            url,
            auth=uplink.auth.ApiTokenHeader("x-gravitee-api-key", api_noekkel),
        )

    """Klient for KKRs REST-API med API-nøkkel i topptekst (x-gravitee-api-key)

    https://oidc-ver2.difi.no//kontaktinfo-oauth2-server/swagger-ui/index.html
    """
    pass
