# Copyright 2022-present Per Christian Gaustad <pcgaustad@mailbox.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
import click

from fregbulk import core
from fregbulk.__about__ import __version__


@click.command()
@click.argument(
    "sett_av_opplysninger",
    type=click.Choice(
        ["navn_status_kontaktadr", "navn_kontaktadr", "regstatus_doedsdato"],
        case_sensitive=False,
    ),
)
@click.argument(
    "innfil",
    type=click.File("r", encoding="utf-8"),
)
@click.argument(
    "utfil",
    type=click.File("w", encoding="utf-8"),
)
@click.option(
    "--format",
    "format_",
    help="format på utdata (standard: csv). (Ved JSON-"
    "format er utdata lik responsen fra Skatteetatens endepunkt.) "
    "Utdataformat kan også bestemmes av UTFIL-ens filendelse.",
    type=click.Choice(["csv", "json"], case_sensitive=False),
    default="csv",
)
@click.option(
    "--kkr",
    "med_oppl_fra_kkr",
    help="Inkluder opplysninger fra KKR",
    is_flag=True,
)
@click.option(
    "--fregoppl",
    help="Les data fra en JSON-fil (generert med "
    "`--format=json`) i stedet for fra oppslag",
    metavar="JSON_FIL",
    type=click.File("r", encoding="utf-8"),
)
@click.option(
    "-v",
    "--verbose",
    help="Terskel for å vise loggmeldinger i kommandolinjen. Angi flere "
    "ganger for mer verbositet (lavere loggnivå).",
    count=True,
)
@click.option(
    "--skilletegn-inn",
    help="skilletegn i inndata (standard: «,»)",
    default=",",
    metavar="SEP",
)
@click.option(
    "--skilletegn-ut",
    help="skilletegn i evt. CSV-utdata (standard: «,»)",
    default=",",
    metavar="SEP",
)
@click.help_option("-h", "--help")
@click.version_option(version=__version__, prog_name="fregbulk")
def fregbulk(
    sett_av_opplysninger,
    innfil,
    utfil,
    med_oppl_fra_kkr,
    format_,
    fregoppl,
    verbose,
    skilletegn_inn,
    skilletegn_ut,
):
    """Slå opp fødselsnummer fra INNFIL i folkeregisteret og lagre et sett av
    opplysninger i UTFIL.

    INNFIL er en CSV-fil med fødselsnummer i første kolonne.

    Applikasjonen bruker som standard konfigurasjonen for development-miljøet i
    config.toml og .secrets.toml. For å bruke konfigurasjonen for
    production-miljøet angi `ENV_FOR_DYNACONF=production`.
    Se https://www.dynaconf.com/#layered-environments-on-files
    """

    import logging.config

    import dynaconf  # type: ignore

    config = dynaconf.Dynaconf(
        settings_files=["config.toml", ".secrets.toml"],
        environments=True,
        merge_enabled=True,
    )

    click.echo(config.logging.loggers[""].level)
    # Sett loggnivå
    if verbose == 1:
        config.logging.loggers[""].level = "WARNING"
    elif verbose == 2:  # noqa: PLR2004
        config.logging.loggers[""].level = "INFO"
    elif verbose >= 3:  # noqa: PLR2004
        config.logging.loggers[""].level = "DEBUG"

    logging.config.dictConfig(config["logging"])

    # logging.error(f'{config.logging.loggers[""].level=}')
    # logging.debug("DEBUG")
    # logging.info("INFO")
    # logging.warning("WARNING")

    core.main(
        sett_av_opplysninger=sett_av_opplysninger,
        innfil=innfil,
        utfil=utfil,
        med_oppl_fra_kkr=med_oppl_fra_kkr,
        format_=format_,
        fregoppl=fregoppl,
        # verbose=verbose,
        skilletegn_inn=skilletegn_inn,
        skilletegn_ut=skilletegn_ut,
        config=config,
    )
    # click.echo("Hello world!")
