# Copyright: 2023-present Per Christian Gaustad <pcgaustad@mailbox.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
__version__ = "0.0.1"
