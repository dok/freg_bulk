# Copyright: 2023-present Per Christian Gaustad <pcgaustad@mailbox.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
import sys

if __name__ == "__main__":
    from fregbulk.cli import fregbulk

    sys.exit(fregbulk())
