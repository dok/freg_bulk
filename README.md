`freg_bulk` er en kommandolinjeapplikasjon for å hente folkeregisteropplysninger for flere personer på én gang.

# Forutsetninger

* En nyere Python-versjon med [Hatch](https://hatch.pypa.io) (testet med Ubuntu 22.02 (WSL) og Python 3.10.12)
* Tilgang til [folkeregister-API-et](https://api-uib.intark.uh-it.no/catalog/api/1fa3a167-7f8d-42f1-a3a1-677f8dc2f16d?q=freg) (for hendelser) i Gravitee.
* Hjemmel til å hente opplysningene du vil ha.


## Sikkerhet

Kommer\ …

## Hva har UiB lov å bruke folkeregisteret til?

*Dette avsnittet må gjennomgås av en fagkyndig.*

Etter folkeregisterloven §\ 10-1 første avsnit kan «offentlige myndigheter og virksomheter […] få utlevert ikke-taushetsbelagte opplysninger fra Folkeregisteret.» Hvor grensen for bruken av disse går, kan være noe uklart,[^hjemmel] men det går frem av forarbeidene til loven at det skal «være mulig å ajourholde eksisterende kundelister ved å vaske disse mot Folkeregisteret, forutsatt at kundelistene tilfredsstiller kravene til identifisering av den enkelte person.»[^prop] Dette må da også gjelde for andre lister (som deltakerlister i et forskningsprosjekt) såfremt de har et lovlig grunnlag.

[^hjemmel]: Skatteetaten [skriver](https://web.archive.org/web/20220910134112/https://www.skatteetaten.no/deling/folkeregisteret/intro/finne-data/) på sine nettsider at «[v]irksomheten kan motta og bruke folkeregisteropplysninger for utførelse av offentlig myndighetsoppgaver eller annen offentlig virksomhet.»

[^prop]: Prop.\ 164\ L (2015–2016) Lov om folkeregistrering (folkeregisterloven), s. 50; jf. også §\ 10-1 annet avsnitt.

UiB har rettighetspakken «offentlig uten hjemmel». Virksomheten har trolig ikke hjemmel til å hente ut «[i]kke-taushetsbelagte opplysninger om en nærmere angitt gruppe av personer […] til forskningsformål», jf. folkeregisterloven §\ 10-3 annet avsnitt. (Det vil si ikke bare vaske opplysninger som vi allerede har.) Per september 2022 må man søke Skattetaten om tillatelse, og så produserer et eventuelt uttrekk.[^forskning]

[^forskning]: Se under overskriften «Forskning» på <https://www.skatteetaten.no/deling/folkeregisteret/intro/finne-data/>.

UiB kan produsere uttrekk med kriteriene som er beskrevet [her](https://skatteetaten.github.io/folkeregisteret-api-dokumentasjon/uttrekk/), til formål som dekkes av §\ 10-1.


# Kom i gang

Last ned Git-arkivet:

```bash
git clone git@git.app.uib.no:uib-dok/freg_bulk.git
cd freg_bulk
```

Filen `config.toml` bruker endepunktene for folkeregisteret og kontaktregisteret fra UiBs API-portal. Endre disse ved behov.

Opprett filen `.secrets.toml` og legg til API-nøkler for endepunktene:

```toml
[development]
freg_api.api_noekkel = ""
kkr_api.api_noekkel = ""

[production]
freg_api.api_noekkel = ""
kkr_api.api_noekkel = ""
```


# Bruk

```
$ hatch run default:python src/fregbulk --help

Usage: fregbulk [OPTIONS]
                {navn_status_kontaktadr|navn_kontaktadr|regstatus_doedsdato}
                INNFIL UTFIL

  Slå opp fødselsnummer fra INNFIL i folkeregisteret og lagre et sett av
  opplysninger i UTFIL.

  INNFIL er en CSV-fil med fødselsnummer i første kolonne.

  Applikasjonen bruker som standard konfigurasjonen for development-miljøet i
  config.toml og .secrets.toml. For å bruke konfigurasjonen for production-
  miljøet angi `ENV_FOR_DYNACONF=production`. Se
  https://www.dynaconf.com/#layered-environments-on-files

Options:
  --format [csv|json]   format på utdata (standard: csv). (Ved JSON-format er
                        utdata lik responsen fra Skatteetatens endepunkt.)
                        Utdataformat kan også bestemmes av UTFIL-ens
                        filendelse.
  --kkr                 Inkluder opplysninger fra KKR
  --fregoppl JSON_FIL   Les data fra en JSON-fil (generert med
                        `--format=json`) i stedet for fra oppslag
  -v, --verbose         Terskel for å vise loggmeldinger i kommandolinjen.
                        Angi flere ganger for mer verbositet (lavere
                        loggnivå).
  --skilletegn-inn SEP  skilletegn i inndata (standard: «,»)
  --skilletegn-ut SEP   skilletegn i evt. CSV-utdata (standard: «,»)
  -h, --help            Show this message and exit.
  --version             Show the version and exit.
```


Applikasjonen leser fødsels- og D-nummer fra første kolonne en CSV-fil (eller stdin). Andre data ignoreres.

Eventuelle duplikate identifikatorer gir duplikate treff.

Ved utgåtte identifikatorer eller identifikatorer som ikke finnes, får man en feilmelding i responsen i stedet for persondata (se eksempelet nedenfor).

Med argumentet `--format=json` får du svaret fra API-et uten at det er bearbeidet.

Med underkommandoen og argumentet `kontaktadr --fra-json=FILNAVN` kan du da bruke dette svaret flere ganger uten å bruke API-et

For å hente opplysninger fra det ekte folkeregisteret (ikke bare testdata) må du bruke konfigurasjonen for production-miljøet. Eksempel:

```bash
hatch run default:ENV_FOR_DYNACONF=production python src/fregbulk --help
```

Personstatusen «aktiv» brukes for personer med personstatus forsvunnet, fødselsregistrert, midlertidig og ikkeBosatt (som er taushetsbelagte).[^aktiv]

[^aktiv]: *Informasjonsmodell - Modernisering av Folkeregisteret* v.\ 4.0 s.\ 29.

## Fremtidsplaner

Se <CHANGELOG.md>.


## Eksempel med testdata (inkludert)

NB: Her er det tydeligvis noe tull med at feltet for fornavn ikke vises.

```
$ hatch run default:python src/fregbulk navn_kontaktadr tests/testdata.csv -
DEBUG
[06/10/24 22:43:13] ERROR    config.logging.loggers[""].level='DEBUG'                                                    __init__.py:112
                    DEBUG    DEBUG                                                                                       __init__.py:113
                    INFO     INFO                                                                                        __init__.py:114
                    WARNING  WARNING                                                                                     __init__.py:115
                    INFO     Les fødsels- og D-nummer fra filen tests/testdata.csv                                           core.py:723
                    DEBUG    Sjekk hvilke verdier i første kolonne som består av 11 siffer …                                 core.py:726
                    INFO     Innholdet i 24 av 25 rad(er) (inkl. evt. kolonnetittel) er et fødsels- eller D-nr.              core.py:737
                    INFO     Hent folkeregisteropplysninger med API-kall …                                                   core.py:105
                    INFO     Del opp liste i bolker med maks 1000 fnr./D-nr. …                                                core.py:70
                    INFO     25 fnr. delt opp i 1 liste(r).                                                                   core.py:55
                    INFO     Utfør API-kall …                                                                                 core.py:73
                    DEBUG    Starting new HTTPS connection (1): gw-uib.intark.uh-it.no:443                        connectionpool.py:1048
[06/10/24 22:43:14] DEBUG    https://gw-uib.intark.uh-it.no:443 "POST                                              connectionpool.py:546
                             /freg/test/v1/personer/bulkoppslag?somBestilt=true&part=navn&part=bostedsadresse&part
                             =oppholdsadresse&part=deltBosted&part=postadresse&part=postadresseIUtlandet HTTP/1.1"
                             200 None
                    DEBUG    API-kall 1 av 1 var vellykket.                                                                   core.py:79
                    INFO     Fjern unødvendige felt i resultatet …                                                           core.py:165
                    INFO     Formater opplysninger …                                                                         core.py:778
                    WARNING  Feilmelding: person={'foedselsEllerDNummer': '00000000000', 'feilmelding': {'feilmelding':      core.py:217
                             'Identifikatoren finnes ikke.'}}
                    INFO     Velg kontaktadresse og formaterer adresser på enhetlig vis …                                    core.py:782
foedselsEllerDNummer,fornavn,mellomnavn,etternavn,forkortetNavn,navnMedStorForbokstav,adresselinje1,adresselinje2,postnummer,poststed,landkode,adressegradering
26894798857,,,BREGNE,,Rosa Bregne,PB Test 234,,4050,SOLA,,ugradert
24847299521,,,ARTISJOKK,,Tom Artisjokk,Kampheimveien 8,,0685,OSLO,,ugradert
17896797220,,,FARGE,,Minimalistisk Farge,Remetunet 9,,6154,ØRSTA,,ugradert
08883047789,,,FIBER,,Parisk Fiber,Nesmoen 3,,4745,BYGLAND,,ugradert
01903949631,,,ALUMINIUM,,Musikalsk Aluminium,Kristoffer Robins vei 24,,0978,OSLO,,ugradert                                                                                                                     28913048794,,,EPLEKAKE,,Slapp Eplekake,PB Test 251,,2323,INGEBERG,,ugradert
05923448618,,,PRIS,,Fantasiløs Pris,c/o Robust Karosseri,PB Test 22,4903,TVEDESTRAND,,ugradert                                                                                                                 22833248905,,,FORELDER,,Uforgjengelig Forelder,Grotbekkvegen 24,,3679,NOTODDEN,,ugradert                                                                                                                       05927999775,,,BJØRN,,Skamfull Bjørn,c/o Varm Hare,PB Test 38,8534,LILAND,,ugradert
21904098520,,,FATTIGMANN,,Momentan Fattigmann,PB Test 252,,0256,OSLO,,ugradert
11892147940,,,OVERGANG,,Formbar Overgang,c/o Skravlete Agenda,PB Test 271,1815,ASKIM,,fortrolig                                                                                                                10904898649,,,BILLE,,Fiktiv Bille,PB Test 258,,0765,OSLO,,ugradert
10913848697,,,FJÆR,,Famøs Fjær,Granlia 12D,,6814,FØRDE,,ugradert
26832649529,,,MÅKE,,Gul Måke,c/o Blid Barneskole,PB Test 211,4588,KVÅS,,ugradert
02860099996,,,INFORMASJON,,Krass Informasjon,Schæffers gate 12A,,0558,OSLO,,ugradert
03882347566,,KLOSETT,GRANNE,,Motløs Klosett Granne,Stavenga 20,,2319,HAMAR,,ugradert                                                                                                                           27913149640,,,PYJAMASBUKSE,,Hensynsløs Pyjamasbukse,PB Test 219,,3912,PORSGRUNN,,ugradert                                                                                                                      07904997859,,,ALM,,Humoristisk Alm,Totland 54,,5993,OSTEREIDET,,ugradert
06852247906,,,MODELL,,Oppjaget Modell,c/o Vårlig Nøtt,PB Test 18,4827,FROLANDS VERK,,ugradert
28842249351,,,KJØTTDEIG,,Fattet Kjøttdeig,PB Test 131,,6011,ÅLESUND,,ugradert
23908299922,,,BILLETT,,Rettferdig Billett,c/o Bestemt Fiskebolle,PB Test 136,4130,HJELMELAND,,ugradert                                                                                                         06883948805,,,VANDREFALK,,Tykkhudet Vandrefalk,PB Test 157,,7896,BREKKVASSELV,,ugradert
00000000000,,,,,,,,,,,
08817196307,,,SYSTEM,,Dynamisk System,,,,,,
```


# Om kontaktadresser (underkommando `kontaktadr`)

## Valg av kontaktadresse

I folkeregisteret er det ingen standard kontaktadresse. Før kunne den registrerte selv velge hvilken adresse som skulle være kontaktadresse. Dersom hen ikke gjorde det, ble den første adressen i listen nedenfor som vedkommende var registrert med, valgt som kontaktadresse.

Underkommandoen `kontaktadr` velger den på samme måte:

> 1. Postadresse
> 1. Oppholdsadresse (om adresseErUkjent = true gå til neste)
> 1. Bostedsadresse (om adressen er UkjentBosted gå til neste)
> 1. Postadresse utland

Kilde: *Informasjonsmodell - Modernisering av Folkeregisteret* v.\ 4.0 s.\ 109.


## Adressegradering

Fra: Informasjonsmodell – Modernisering av Folkeregisteret, s. 109 av 123

> Adresseopplysninger i registeret (bosted, opphold og post) har element for
> adressegradering og kan ha verdiene ugradert, klientadresse, fortrolig.

Gjeldende fortrolige adresser kan utleveres i rettighetspakken «offentlig uten hjemmel». Strengt fortrolige adresse og klientadresser utleveres ikke.

Kilde: *Informasjonsmodell - Modernisering av Folkeregisteret* v.\ 4.0 s.\ 108. 


## Adresseformat

Applikasjonen forsøker å gi alle kontaktadresser et enhetlig, slik at de enkelt kan settes inn i en tabell. Den tar utgangspunkt i følgende adresseformat:

Fra Digdir Docs’ [begrepskatalog](https://docs.digdir.no/resources/begrep/sikkerDigitalPost/begrep/FysiskPostadresse):

> FysiskPostadresse er enten en NorskPostadresse eller en UtenlandskPostadresse.
>
> #### NorskPostadresse:
>
> | Term | Kardinalitet | Datatype | Regler |
> | --- | --- | --- | --- |
> | [navn](http://www.w3.org/TR/xmlschema-2/#string)           | 1..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 130 tegn |
> | [adresselinje 1](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [adresselinje 2](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [adresselinje 3](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [postnummer](http://www.w3.org/TR/xmlschema-2/#string)     | 1..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | 4 siffer      |
> | [poststed](http://www.w3.org/TR/xmlschema-2/#string)       | 1..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 80 tegn  |
>
>
> #### UtenlandskPostadresse:
>
> | Term | Kardinalitet | Datatype | Regler |
> | --- | --- | --- | --- |
> | [navn](http://www.w3.org/TR/xmlschema-2/#string)           | 1..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 130 tegn |
> | [adresselinje 1](http://www.w3.org/TR/xmlschema-2/#string) | 1..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [adresselinje 2](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [adresselinje 3](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
> | [adresselinje 4](http://www.w3.org/TR/xmlschema-2/#string) | 0..1         | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks 100 tegn |
>
> i tillegg et av følgende to felt:
>
> |   |   |   |   |
> | --- | --- | --- | --- |
> | [landkode](http://www.w3.org/TR/xmlschema-2/#string) | 1..1 | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Anbefalt brukt fremfor land. To-bokstavs landkode ihht [ISO 3166-1 alpha-2 standarden](http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) |
> | [land](http://www.w3.org/TR/xmlschema-2/#string)     | 1..1 | [xs:string](http://www.w3.org/TR/xmlschema-2/#string) | Maks. 80 siffer, brukes dersom Avsender ikke har mulighet til å benytte landkode.

Fra [dokumentasjon av SvarUt](https://ks-no.github.io/svarut/integrasjon/forsendelserestservicev1/) fra KS:

> #### PostAdresse
>
> | Felt       | Type    | Beskrivelse      | Validering     |
> | ---------- | ------- | ---------------- | -------------- |
> | navn       | String  | Navn på mottaker | Må være utfylt. |
> | adresse1   | String  | Adresselinje1    |                |
> | adresse2   | String  | Adresselinje2    |                |
> | adresse3   | String  | Adresselinje3    |                |
> | postSted   | String  | Poststed         | Må være utfylt dersom forsendelsen ikke er kun digital. |
> | postNummer | String  | Postnummer       | Må være utfylt dersom forsendelsen ikke er kun digital. Fire siffer for norske adresser. |
> | land       | String  | Land             | Må være utfylt dersom forsendelsen ikke er kun digital. |


# Lenker

* [Skattetatens API-dokumentasjon for folkeregisteret](https://skatteetaten.github.io/folkeregisteret-api-dokumentasjon)
* [Informasjonsmodell for folkeregisteret](https://skatteetaten.github.io/folkeregisteret-api-dokumentasjon/informasjonsmodell/)
* [*Informasjonsmodell - Modernisering av Folkeregisteret* v.\ 4.0](http://skatteetaten.github.io/folkeregisteret-api-dokumentasjon/dokumenter/20220916_Informasjonsmodell-Modernisering%20av%20folkeregisteret%20v4.0.pdf)
* [OpenAPI-dokumentasjon for rettighetspakken «offentlig uten hjemmel»](https://app.swaggerhub.com/apis/Skatteetaten_FREG/RettighetspakkerUtenTaushetsbelagtOff)
* [Testdata for folkeregisteropplysninger](https://www.skatteetaten.no/skjema/testdata/)
